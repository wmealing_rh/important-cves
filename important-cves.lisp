;;;; important-cves.lisp

(in-package #:important-cves)

;; todo, move these.
(ql:quickload "cl-jira")
(ql:quickload "arrows")
(ql:quickload "str")
(ql:quickload "alexandria")
(ql:quickload "lparallel")
(ql:quickload "hiccl")
(ql:quickload "cl-slug")
(ql:quickload "local-time")
(ql:quickload "lparallel")
(ql:quickload "hunchentoot")

(load "config.lisp")

(defvar  *acceptor* (make-instance 'hunchentoot:easy-acceptor :port 4242))

(push (hunchentoot:create-folder-dispatcher-and-handler
       "/static/"
       (truename "/app/static/"))
      hunchentoot:*dispatch-table*)

(defun start-server ()
  (format t "~&Starting the web server~%")
  (force-output)
  (hunchentoot:start *acceptor*))

(defun stop-server ()
  (format t "~&Stopping the web server~%")
  (force-output)
  (hunchentoot:stop *acceptor*))


;; 12 seems optimal for me.


(defparameter *my-hash* (make-hash-table :test 'equal))

(defparameter importants-query "project = RHEL and labels = SecurityTracking and status NOT IN (Done, Resolved, Obsolete, Blocked, Closed, Rejected, Dropped) and priority = Major and labels = pscomponent:kernel")

(defun get-importants ()
  (jira:query-with-raw importants-query))

(defun get-key (issue)
  (getf issue :|key|))

(defun issue-list ()
  (arrows:->>
    (getf (get-importants)  :|issues|)
    (mapcar (lambda (x) (get-key x)))))

(defun strip-version (summary-text)
  (first (str:split " [" summary-text)))

(defun parse-issue-summary (issue-data)
  (strip-version (cl-jira:issue->summary issue-data)))

(defun get-version-as-string (jira-data)
  (let ((version (cl-jira:issue->fixversion jira-data)))
    (if (not version)
        "NO-VERSION-SET"
        version
        )))

(defun issue-and-summary (jira-id)
  (force-output)
  (let ((jira-data (cl-jira:get-issue jira-id)))
    `(:name ,jira-id
      :summary ,(parse-issue-summary jira-data)
      :version ,(get-version-as-string jira-data)
      :resolve-by ,(cl-jira:issue->resolve-by jira-data)
      :jira-status ,(cl-jira:issue->status jira-data)
      :prodsec-bz-label ,(cl-jira:issue->prodsec-flawlabel jira-data)
      :rt-status ,(important-cves.realtime:validate jira-data)
      )))

(defun issue-and-summary-list (issue-list)
  (lparallel:pmapcar 'issue-and-summary issue-list) )


(defun unique-flaw-labels (issue-list)
  (remove-duplicates
   (mapcar (lambda (x)
             (getf x :prodsec-bz-label)
             ) issue-list)
   :test #'equal))

(defun groups-of-label (label issue-list)
  (delete 'nil
          (mapcar (lambda (x)
                    (if (equal (getf x :prodsec-bz-label) label)
                        x)) issue-list)))

;; this is not in quicklisp, yet, GAAARLLIIIIC

(defun html-component  (body)
  `(:html
    (:head
     (:title "IMPORTANT REPORT")
     (:link :rel :stylesheet :href "static/style.css")
     (:meta :http-equiv "refresh" :content "600"))
    ,body    ))

(defun jira-to-link (jira-id)
  (concatenate 'string "https://issues.redhat.com/browse/" jira-id))

(defun issue-breakdown-component (issue-data)
  (destructuring-bind
      (&key
         name summary version resolve-by
         jira-status prodsec-bz-label rt-status
         ) issue-data
    `(:table :class "jira-item"
             (:tr
              (:th ,version))
             (:tr
              (:td :class ,(cl-slug:slugify jira-status ) ,jira-status))
             (:tr
              (:td ,(concatenate 'string "Ship by: " resolve-by)))
             (:tr
              (:td
               (:a
                :href ,(jira-to-link name) ,name)
               ))
             (:tr
              (:td ,rt-status ))
             )
    ))

;; group-by-key function.
(defun keyfunc (d) (subseq (getf d :VERSION) 0 6))




(defun group-issues (issue-group)
  (group-by:group-by issue-group :key 'keyfunc :value #'identity))

(defun version-key (a)
  (getf a :VERSION)
  )

(defun minor-version-key (a)
  (if (string= (getf a :VERSION) "NO-VERSION-SET")
      0
      (parse-number:parse-number 
       (subseq (getf a :VERSION) 7 9 ))))

(defun issue-sub-group-component (issue-sub-group)
  `(:div :class "release"
         ,@(loop for jira in (sort (rest issue-sub-group)  #'> :key #'minor-version-key)
                 collect (issue-breakdown-component jira))))

(defun issue-group-component (issue-group)
  `(:div :class "issue-group"
         (:div :class "issue-summary"
               ,(getf (first issue-group) :summary))
         (:div :class "version-group"
               ,@(loop for sub-group in (group-issues issue-group)
                       collect (issue-sub-group-component sub-group)
                       ))))

;; (defun issue-group-component (issue-group)
;;   `(:div :class "issue-group"
;;          (:div :class "issue-summary"
;;                ,(getf (first issue-group) :summary))
;;          (:div :class "issue-breakdown"
;;                (:<> ,@(loop for jira in issue-group
;;                             collect (issue-breakdown-component jira)))
;;                )
;;          ))


(defun timestring-now ()
  (local-time:format-timestring nil (local-time:now)
                                :format local-time:+asctime-format+))

(defun htmlize (issue-groups)
  (hiccl:render nil
    (html-component
     `(:div
       (:h1 ,(concatenate 'string "Important flaws as of " (timestring-now)))
       (:<> ,@(loop for group in issue-groups
                    collect `(:div ,(issue-group-component group))))))))

(defparameter *important-html-file* "/app/static/index.html")

(defun wtf (input)
  (with-open-file (str *important-html-file*
                       :direction :output
                       :if-exists :supersede
                       :if-does-not-exist :create)
    (format str "~a~%" input)))

(defun live-grouped-by-label  ()
  ;; get a list of all flaws
  ;; get a list of all labels
  ;; group them by label

  (let* ((all-flaws (issue-and-summary-list (issue-list)))
         (all-labels (unique-flaw-labels all-flaws))
         (grouped-by-label (mapcar (lambda (label)
                                     (groups-of-label label all-flaws)
                                     ) all-labels )))
    grouped-by-label))

(hunchentoot:define-easy-handler (slash-route :uri "/") ()
  (setf (hunchentoot:content-type*) "text/html")
  (uiop:read-file-string "/app/static/index.html"))

(defun main ()
  (setf lparallel:*kernel* (lparallel:make-kernel 12))
  (force-output)
  (start-server)
  (cl-jira:set-api-key (uiop:getenv "JIRA_KEY"))
  (format t "GOT JIRA KEY: ~a"  (uiop:getenv "JIRA_KEY"))
  (force-output)

  ;;  (wtf (htmlize (live-grouped-by-label)))
  
  (loop do (progn
             (sleep (* 60 10))
             (wtf (htmlize (live-grouped-by-label))))))


;; generate the html.


;; generate teh file.
;; (wtf (htmlize (live-grouped-by-label)))
;; (time (wtf (htmlize (live-grouped-by-label))))


;;(defun keyfunc (d) (subseq (getf d :VERSION) 0 6))
;; (group-by:group-by mm3 :key 'keyfunc)

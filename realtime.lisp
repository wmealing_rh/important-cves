(defpackage #:important-cves.realtime
  (:use #:cl)
  (:export #:find-sibling #:escape-label #:validate))

(in-package #:important-cves.realtime)

(defparameter base-sibling-query "project = RHEL and labels = SecurityTracking and labels = pscomponent:kernel-rt ")

(ql:quickload "cl-arrows")
(ql:quickload "str")

;; jira wants labels escaped.. why ?
(defun escape-label (label)
  (str:replace-first "#" "\\u0023" label))

(defun find-sibling (jira-data)
  (let ((flaw-label (cl-jira:issue->prodsec-flawlabel jira-data))
        (fix-version (cl-jira:issue->fixversion jira-data)))

    ;; the original bug has no fix version !
    (if (eql fix-version nil)
        (return-from find-sibling 'nil))
    (cl-arrows:->
        (cl-arrows:-> base-sibling-query
            (str:concat " and labels = " (escape-label flaw-label))
            (str:concat " and fixversion = " fix-version)
            (jira:query-with-raw))
        (getf :|issues|)
        (first)
        (getf :|key|)
        )))

(defun make-jira-link (jira-id msg)
  `(:a :href ,(concatenate 'string "http://issues.redhat.com/browse/" jira-id) ,msg))

(defun validate (kernel-issue-data)

  (let* ((rhel-jira-id (cl-jira:issue->key kernel-issue-data))
         (jira-data (cl-jira:get-issue rhel-jira-id))
         (rt-jira-id (find-sibling jira-data))
         (rt-jira-version (cl-jira:issue->fixversion jira-data)))

    ;; maybe should make this configurable ?
    (if (string= rt-jira-version "rhel-8.8.0.z")
        (return-from validate
          "NO RT RQRD"))

    (if (string= rt-jira-version "rhel-7.7.0.z")
        (return-from validate
          "NO RT RQRD"))

    (if (string= rt-jira-version "rhel-7.6.0.z")
        (return-from validate
          "NO RT RQRD"))

    (if (not rt-jira-id)
        (return-from validate
          "Missing RT JIRA"))

    (if (not (string= (cl-jira:issue->status kernel-issue-data)
                      (cl-jira:issue->status (cl-jira:get-issue rt-jira-id))))
        (return-from validate (make-jira-link rt-jira-id "RT status mismatch")))

    (return-from validate (make-jira-link rt-jira-id "RT OK"))))

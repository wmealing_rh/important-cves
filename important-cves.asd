;;;; important-cves.asd

(asdf:defsystem #:important-cves
  :description "Describe important-cves here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :depends-on (:cl-jira :arrows :cl-slug :lparallel :hiccl :group-by :hunchentoot)
  :serial t
  :components ((:file "package")
               (:file "html") ;; report.
               (:file "realtime") ;; realtime related queries.
               (:file "important-cves")))

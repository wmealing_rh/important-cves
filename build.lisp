(in-package :cl-user)

(ql:quickload :change-component)

(sb-ext:save-lisp-and-die "change-component"
                          :toplevel 'change-component:change-component-main
                          :executable t
                          :compression t)


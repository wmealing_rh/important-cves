SBCL := `which sbcl`


all:
	qlot exec sbcl --load important-cves.asd --eval '(ql:quickload :important-cves)' --eval "(sb-ext:save-lisp-and-die #p\"important-cves\" :toplevel #'important-cves:main :executable t)"

clean:
	rm -rf important-cves

build-container:
	podman build . -t important-cves 

run-container:
	podman run --env 'JIRA_KEY*'   --rm -it  localhost/important-cves:latest
